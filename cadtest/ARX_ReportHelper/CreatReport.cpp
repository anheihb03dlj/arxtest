#include "stdafx.h"
#include "Resource.h"

#include "CmyWord.h"
#include "CreatReport.h"

//#include "ReportFileDlg.h"

//选择文件对话框所需头文件和动态链接库
//#include <shlwapi.h>
//#pragma comment(lib,"Shlwapi.lib") //也可以在项目属性中设置

#include <iostream>
#include <fstream>
using namespace std;

//AcFStream.h一定要放在iostream，fstream等标准库头文件的后面!!!(切记)
#include "AcFStream.h"

static CString GetAppPathDir()
{
	TCHAR szModulePath[_MAX_PATH];
	GetModuleFileName( _hdllInstance, szModulePath, _MAX_PATH );

	TCHAR drive[_MAX_DRIVE];
	TCHAR dir[_MAX_DIR];
	_tsplitpath( szModulePath, drive, dir, NULL, NULL );

	TCHAR szPath[_MAX_PATH] = {0};
	_tmakepath( szPath, drive, dir, NULL, NULL );

	return CString( szPath );
}

static CString BuildPath( const CString& dir, const CString& fileName )
{
	CString path;
	path.Format( _T( "%s%s" ), dir, fileName );
	return path;
}

static void SetFieldsValue()
{
	//把你生成的数据文件固定在datas路径下
	//文件名固定为vars.txt即可
	CString dataDirName = _T( "datas\\" );
	CString fileName =BuildPath ( BuildPath( GetAppPathDir(), dataDirName ),_T( "vars.txt" ) );

	//不要用你之前的那个win32函数GetModuleFileName获取路径
	//用我的代码中的方法即可(GetAppPathDir()函数)
	//否则编译会有一些诡异的问题

	AcIfstream inFile(fileName);
	//acutPrintf(_T("\n%s"),fileName);
	if(!inFile) return;
	while( !inFile.eof() )
	{
		TCHAR valueField[MAX_PATH], value[MAX_PATH];
		inFile >> valueField >> value;
		acutPrintf(_T("\n名字:%s,值:%s"),valueField,value);
		MyWord->FindWord(valueField,value);
	}
	//MyWord->InsertImage("D:\\test\\cpptest\\ARXTest\\TVNS\\RunGUI\\images\\background.png");
	//MyWord->SetHeaderAndFooter("xx","yy");
	//MyWord->FindWord("{{Coal_Face}}","xxxx工作面");
	//MyWord->FindWord("{{Mine_Name}}","YY矿");
	//MyWord->FindWord("{{Water_Value}}","1x4.5");
}
static void SaveReport(CString savePath)
{
	//char szFileFilter[] = "doc文档(*.doc)|*.doc||";
	//char szFileExt[] = "doc";	
	//CFileDialog dlg(FALSE,szFileExt,savePath+"评测报告",OFN_HIDEREADONLY,szFileFilter);///TRUE为OPEN对话框，FALSE为SAVE AS对话框

	//if(IDOK == dlg.DoModal())
	//{
	//	CString selectedPath = dlg.GetPathName();
	//	if (PathFileExists(selectedPath))
	//	{
	//		if(IDYES == AfxMessageBox("文件已经存在，是否替换?",MB_YESNO|MB_ICONQUESTION))
	//		{
	//			::DeleteFile(selectedPath);
	//			AfxGetMainWnd()->BeginWaitCursor();//设置等待光标
	//			MyWord->SaveAs(selectedPath,wdFormatDocument);
	//			AfxGetMainWnd()->EndWaitCursor();//设置等待光标
	//		}
	//		else
	//		{
	//			AfxMessageBox("保存失败!",MB_OK|MB_ICONSTOP);
	//		}
	//	}
	//	else //不存在
	//	{
	//		AfxGetMainWnd()->BeginWaitCursor();//设置等待光标
	//		MyWord->SaveAs(selectedPath,wdFormatDocument);
	//		AfxGetMainWnd()->EndWaitCursor();//设置等待光标
	//	}
	//}

	//if (PathFileExists(savePath))
	//{
	//	if(IDYES == AfxMessageBox("文件已经存在，是否替换?",MB_YESNO|MB_ICONQUESTION))
	//	{
	//		::DeleteFile(savePath);
	//		AfxGetMainWnd()->BeginWaitCursor();//设置等待光标
	//		MyWord->SaveAs(savePath,wdFormatDocument);
	//		AfxGetMainWnd()->EndWaitCursor();//设置等待光标
	//	}
	//	else
	//	{
	//		AfxMessageBox("文件名重复!",MB_OK|MB_ICONSTOP);
	//	}
	//}
	//else //不存在
	//{
	//	AfxGetMainWnd()->BeginWaitCursor();//设置等待光标
	//	MyWord->SaveAs(savePath,wdFormatDocument);
	//	AfxGetMainWnd()->EndWaitCursor();//设置等待光标
	//}
	MyWord->SaveAs(savePath);

}

void wordOprate(CString templPath,CString savePath)
{
	AfxGetMainWnd()->BeginWaitCursor();//设置等待光标
	MyWord->CreateAPP();
	//CString strPath/*,savePath*/; 
	//strPath= GetAppPathDir();
	//savePath = strPath;
	//strPath=strPath+"\\template\\template.doc";
	if(!MyWord->Open(/*strPath*/templPath))
	{
		MyWord->AppClose();
		return;
	}
	SetFieldsValue();
	SaveReport(savePath);
	MyWord->Close(FALSE);
	MyWord->AppClose();
	AfxGetMainWnd()->EndWaitCursor();//结束等待光标
}

//这个类的其他用法参考
//CString t;
//MyWord->GetWordText(t);
//嘿嘿 C++ 确实好多东西比c 方便多了 
//AfxMessageBox(t);
//MyWord->Save();
//MyWord->WriteText(t);
//MyWord->Save();
//MyWord->SaveAs("C:\\Users\\hd\\Desktop\\word操作封装类\\Debug\\output.doc",wdFormatFilteredHTML);
//AfxMessageBox("要关闭了");
//MyWord->SetFont("宋体",9,26367,0);
//MyWord->SetParaphformat(wdAlignParagraphCenter);
//MyWord->WriteText("小鱼儿 新的一年 新的开始 加油 ");
//MyWord->WriteTextNewLineText("胡萍",5);
//
//   MyWord->CreateTable(4,4);
//MyWord->WriteCellText(1,1,"小鱼儿");
//CComVariant wdLine(5),Count(3),Vopt;
//MyWord->sel.MoveEnd(&wdLine,&Count);
//MyWord->WriteText("小鱼儿 新的一年 新的开始 加油 ");
//return nRetCode;


bool initword()
{
	//CoInitialize返回的是HResult类型的值
	//要用SUCCEDED或FAILED宏来判断成功或失败
	//如果用S_OK或S_FALSE来判断是错误的!!!
	//你可以用VC助手看看这4个宏的定义!!!
	if(FAILED(CoInitialize(NULL)))
	//if(CoInitialize(NULL)!=S_OK) // 错误的判断方法!!!
	{
		AfxMessageBox(_T("初始化com库失败"));
		return false;
	}

	MyWord = new CmyWord;
	return true;
}

void uninitword()
{
	delete MyWord;
	MyWord = 0;

	//释放资源最好从 小到大的顺序来释放。这个和c里面一些释放资源的道理是一样的
	//和c+= 先析构儿子 再析构父亲是一样的。
	CoUninitialize();
}

void CreatReport(const CString& savePath)
{
	//AFX_MANAGE_STATE(AfxGetStaticModuleState());
	//ReportFileDlg fileDlg;
	//if(IDOK == fileDlg.DoModal())
	{
		//固定模板文件名称,没有必要把它做成参数
		CString dataDirName = _T( "datas\\" );
		CString fileName =BuildPath ( BuildPath( GetAppPathDir(), dataDirName ),_T( "tpl.doc" ) );
		//生成报告
		//注意:如果savePath没有写绝对路径的话，那么cad可能会使用"我的文档"作为默认路径
		//这个默认路径与cad的配置有关，具体原因不明确!
		wordOprate(fileName,savePath);
		//wordOprate(fileDlg.m_templatePath,fileDlg.m_savePath);
	}
}
